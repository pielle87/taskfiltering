import { Task } from '../app/DAOs/task';

// NOTE: a bit of a hack here: taskId = 0 is reserved for the PROJECT
export const TASKS: Task[] = [
  // PROJECT 0
  {
    projectId: 0, taskId: 1, parentTaskId: 0,
    name: 'TASK010',
    data: {
      details: 'a',
      comments: 'b',
      volunteers: [
        { name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png'},
        { name: 'Jane Smith', avatar: 'assets/avatars/JaneSmith.png'}
      ],
      rating: 2,
    }
  },
  {
    projectId: 0, taskId: 2, parentTaskId: 0,
    name: 'TASK020',
    data: {
      details:  'short summary of the task',
      description: 'In the same way, I might have a very long description but this is only shown if I select the task',
      volunteers: [
        { name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png'}
      ]
    }
  },
  {
    projectId: 0, taskId: 3, parentTaskId: 1,
    name: 'TASK031', data: {     details: 'this is a very very long DETAILS-section which does not really provide' +
    'any useful information but which I can use to force the UI to add some newlines. Does this look good?',
     description: 'In this case the description is really short', volunteers: [{ name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png' }] }
  },
  {
    projectId: 0, taskId: 4, parentTaskId: 1,
    name: 'TASK041', data: { details: '' }
  },
  {
    projectId: 0, taskId: 5, parentTaskId: 2,
    name: 'TASK052',
    data: {
      details: 'e',
      volunteers: [
        { name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png' },
        { name: 'Jane Smith', avatar: 'assets/avatars/JaneSmith.png'}
      ]
    }
  },
  {
    projectId: 0, taskId: 6, parentTaskId: 4,
    name: 'TASK064', data: { volunteers: [{ name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png' }] }
  },
  // PROJECT 1
  {
    projectId: 1, taskId: 1, parentTaskId: 0,
    name: 'TASK110',
    data: {
      details: 'd',
      volunteers: [
        { name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png' }
      ]
    }
  },
  {
    projectId: 1, taskId: 2,  parentTaskId: 1,
    name: 'TASK121', data: { details: 'd', comments: 'c', volunteers: [{ name: 'Jane Smith', avatar: 'assets/avatars/JaneSmith.png' }] }
  },
  // PROJECT 5
  {
    projectId: 5, taskId: 1, parentTaskId: 0,
    name: 'Vorbereitung Monatsübung',
    data: {
      details: 'Glimmbrand Dachboden mit 2 vermissten, verletzen Personen',
      location: 'Moarhof',
      time: '18:00 - 20:00',
      date: new Date(2019, 2, 30),
      volunteers: [
        { name: 'Markus Schiller', avatar: 'assets/avatars/MarkusSchiller.png' },
      ]
    }
  },
  {
    projectId: 5, taskId: 2, parentTaskId: 0,
    name: 'Durchführung Übung',
    data: {
      location: 'Moarhof',
      time: '19:30 - 21:15',
      date: new Date(2019, 3, 4),
      volunteers: [
        { name: 'Markus Schiller', avatar: 'assets/avatars/MarkusSchiller.png' },
        { name: 'Vera Rassel', avatar: 'assets/avatars/VeraRassel.png' },
        { name: 'Hubert Hauer', avatar: 'assets/avatars/HubertHauer.png' },
        { name: 'Sigi Detsch', avatar: 'assets/avatars/SigiDetsch.png' },
      ]
    }
  },
  {
    projectId: 5, taskId: 3, parentTaskId: 0,
    name: 'Nachbearbeitung',
    data: {
      location: 'Moarhof',
      time: '8:00 - 9:00',
      date: new Date(2019, 3, 10),
      volunteers: [
        { name: 'Markus Schiller', avatar: 'assets/avatars/MarkusSchiller.png' },
        { name: 'Vera Rassel', avatar: 'assets/avatars/VeraRassel.png' },
      ]
    }
  },
  {
    projectId: 5, taskId: 4, parentTaskId: 1,
    name: 'Aufbau Dachboden',
    data: {
      date: new Date(2019, 2, 30),
      volunteers: [
        { name: 'Markus Schiller', avatar: 'assets/avatars/MarkusSchiller.png' },
      ]
    }
  },
  {
    projectId: 5, taskId: 4, parentTaskId: 1,
    name: 'Szenario planen',
    data: {
      date: new Date(2019, 2, 30),
      volunteers: [
        { name: 'Markus Schiller', avatar: 'assets/avatars/MarkusSchiller.png' },
      ]
    }
  },
  // PROJECT 6
  {
    projectId: 6, taskId: 1, parentTaskId: 0,
    name: 'Musikerprobe 1',
    data: {
      details: 'Bläser',
      location: 'Schwertberg Musikhaus',
      date: new Date(2019, 4, 8),
      time: '20:00',
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Martin M', avatar: 'assets/avatars/MartinM.png' },
        { name: 'Sandra', avatar: 'assets/avatars/Sandra.png' },
        { name: 'Ursula', avatar: 'assets/avatars/Ursula.png' },
      ]
    }
  },
  {
    projectId: 6, taskId: 2, parentTaskId: 0,
    name: 'Musikerprobe 2',
    data: {
      details: 'Trommeln',
      location: 'Schwertberg Musikhaus',
      date: new Date(2019, 4, 12),
      time: '10:00',
      volunteers: [
        { name: 'Vera', avatar: 'assets/avatars/Vera.png' },
        { name: 'Simone', avatar: 'assets/avatars/Simone.png' },
        { name: 'Fred', avatar: 'assets/avatars/Fred.png' },
      ],
      rating: 1,
    }
  },
  {
    projectId: 6, taskId: 3, parentTaskId: 0,
    name: 'Orchesterprobe',
    data: {
      location: 'Schwertberg Ortsplatz',
      date: new Date(2019, 4, 15),
      time: '09:00',
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Martin M', avatar: 'assets/avatars/MartinM.png' },
        { name: 'Ursula', avatar: 'assets/avatars/Ursula.png' },
        { name: 'Vera', avatar: 'assets/avatars/Vera.png' },
        { name: 'Simone', avatar: 'assets/avatars/Simone.png' },
      ]
    }
  },
  {
    projectId: 6, taskId: 4, parentTaskId: 0,
    name: 'Marschprobe',
    data: {
      date: new Date(2019, 4, 15),
      time: '10:00',
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Martin M', avatar: 'assets/avatars/MartinM.png' },
        { name: 'Ursula', avatar: 'assets/avatars/Ursula.png' },
        { name: 'Vera', avatar: 'assets/avatars/Vera.png' },
        { name: 'Fred', avatar: 'assets/avatars/Fred.png' },
      ]
    }
  },
  {
    projectId: 6, taskId: 4, parentTaskId: 0,
    name: 'Marsch',
    data: {
      location: 'Schwertberg Marktplatz',
      date: new Date(2019, 4, 18),
      time: '10:00',
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Martin M', avatar: 'assets/avatars/MartinM.png' },
        { name: 'Ursula', avatar: 'assets/avatars/Ursula.png' },
        { name: 'Vera', avatar: 'assets/avatars/Vera.png' },
        { name: 'Simone', avatar: 'assets/avatars/Simone.png' },
        { name: 'Sandra', avatar: 'assets/avatars/Sandra.png' },
        { name: 'Fred', avatar: 'assets/avatars/Fred.png' },
      ]
    }
  },
  // PROJECT 7
  {
    projectId: 7, taskId: 1, parentTaskId: 0,
    name: 'Fr. Mustermann fahren',
    data: {
      details: 'UM 8:00 - VON: Enns (Stifterstrasse 34) - NACH: KH Steyer',
      description: 'Dialyse',
      time: '08:00 - 09:30',
      date: new Date(2019, 2, 25),
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Hubert Hauer', avatar: 'assets/avatars/HubertHauer.png' },
      ]
    }
  },
  {
    projectId: 7, taskId: 2, parentTaskId: 0,
    name: 'Hr. Steinbacher fahren',
    data: {
      details: 'UM 11:30 - VON: Steyr (Taborstr. 5) - NACH: Ordination Dr. Weigl HNO',
      description: 'HNO',
      time: '11:30 - 13:00',
      date: new Date(2019, 2, 25),
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Hubert Hauer', avatar: 'assets/avatars/HubertHauer.png' },
      ]
    }
  },
  {
    projectId: 7, taskId: 3, parentTaskId: 0,
    name: 'Fr. Maurerbach hinfahren',
    data: {
      details: 'UM 15:00 - VON: Altersheim Steyr - NACH: KH Steyer',
      time: '15:00 - 15:45',
      date: new Date(2019, 2, 25),
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
      ]
    }
  },
  {
    projectId: 7, taskId: 4, parentTaskId: 0,
    name: 'Fr. Maurerbach abholen',
    data: {
      details: 'UM 17:00 - VON: KH Steyer - NACH: Altersheim Steyr',
      time: '17:00 - 17:45',
      date: new Date(2019, 2, 25),
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
      ]
    }
  },
  // PROJECT 8
  {
    projectId: 8, taskId: 1, parentTaskId: 0,
    name: 'Workshop RSA Praxistag',
    data: {
      details: 'Jugendliche üben RSA an Testpersonen',
      location: 'Perg',
      volunteers: [
        { name: 'Vera', avatar: 'assets/avatars/Vera.png' },
        { name: 'Simone', avatar: 'assets/avatars/Simone.png' },
        { name: 'Sandra', avatar: 'assets/avatars/Sandra.png' },
        { name: 'Fred', avatar: 'assets/avatars/Fred.png' },
      ],
      rating: 2,
    }
  },
  {
    projectId: 8, taskId: 2, parentTaskId: 1,
    name: 'Prüfung RSA',
    data: {
      details: 'Prüfung zur Erlangung des Zertifikats "RSA-Ausbildung"',
      location: 'Perg',
      volunteers: [
        { name: 'Vera', avatar: 'assets/avatars/Vera.png' },
        { name: 'Simone', avatar: 'assets/avatars/Simone.png' },
        { name: 'Sandra', avatar: 'assets/avatars/Sandra.png' },
        { name: 'Fred', avatar: 'assets/avatars/Fred.png' },
      ],
      rating: 5,
    }
  },
  {
    projectId: 8, taskId: 3, parentTaskId: 0,
    name: 'Workshops Get Social Jugendreporter',
    data: {
      details: 'Jugendliche können in Wort und bild Texte für Presseaussendungen bearbeiten',
      location: 'Schwertberg',
      date: new Date(2019, 0, 26),
      volunteers: [
        { name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png'},
        { name: 'Jane Smith', avatar: 'assets/avatars/JaneSmith.png'}
      ]
    }
  },
  {
    projectId: 8, taskId: 4, parentTaskId: 0,
    name: 'Workshops Get Social Praxis',
    data: {
      description: 'Arbeiten im Team mit Feedback-Einheit',
      location: 'Schwertberg',
      date: new Date(2019, 1, 20),
      volunteers: [
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Martin M', avatar: 'assets/avatars/MartinM.png' },
        { name: 'Ursula', avatar: 'assets/avatars/Ursula.png' },
      ]
    }
  },
  {
    projectId: 8, taskId: 5, parentTaskId: 0,
    name: 'Workshop RD, C10 + RLS',
    data: {
      details: 'Theorieeinheit',
      description: 'Im Anschluss RD in einigen realen Szenarien anwenden',
      location: 'Schwertberg',
      date: new Date(2019, 2, 2),
      volunteers: [
        { name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png'},
        { name: 'Jane Smith', avatar: 'assets/avatars/JaneSmith.png'},
        { name: 'Hannes', avatar: 'assets/avatars/Hannes.png' },
        { name: 'Martin M', avatar: 'assets/avatars/MartinM.png' },
        { name: 'Ursula', avatar: 'assets/avatars/Ursula.png' },
        { name: 'Vera', avatar: 'assets/avatars/Vera.png' },
        { name: 'Simone', avatar: 'assets/avatars/Simone.png' },
        { name: 'Sandra', avatar: 'assets/avatars/Sandra.png' },
        { name: 'Fred', avatar: 'assets/avatars/Fred.png' },
      ]
    }
  },
  // PROJECT 9
  {
    projectId: 9, taskId: 1, parentTaskId: 0,
    name: 'Brandeinsatz in Treffling',
    data: {
      date: new Date(2019, 6, 3),
      comments: 'Der Brand konnte ohne größere Probleme in 15 Min gelöscht werden',
      materials: 'Feuerleiter, Spritzpumpe',
      location: 'Treffling',
      volunteers: [
        { name: 'Thomas', avatar: 'assets/avatars/Thomas.png' },
        { name: 'Hugo', avatar: 'assets/avatars/Hugo.png' },
        { name: 'Maria', avatar: 'assets/avatars/Maria.png' },
        { name: 'Sepp', avatar: 'assets/avatars/Sepp.png' },
      ],
    }
  },
  {
    projectId: 9, taskId: 2, parentTaskId: 0,
    name: 'Katzenrettung in Schlierbach',
    data: {
      date: new Date(2019, 6, 3),
      comments: 'Die Katze konnte unter Mithilfe der Besitzerin vom Baum geborgen werden',
      materials: 'lange Leiter',
      location: 'Schlierbach',
      volunteers: [
        { name: 'Sophie', avatar: 'assets/avatars/Sophie.png' },
        { name: 'Walter', avatar: 'assets/avatars/Walter.png' },
        { name: 'Alfred', avatar: 'assets/avatars/Alfred.png' },
      ],
    }
  },
];
