import { MapMarker } from '../app/DAOs/map-marker';

export const MARKERS: MapMarker[] = [
  { projectId: 0, position: {lat: 41.9, lng: 12.5} },    // Rome
  { projectId: 1, position: {lat: 48.9, lng: 2.4} },     // Paris
  { projectId: 2, position: {lat: 48.31288456824884, lng: 14.294757843017578 } },  // Linz Donaulände
  { projectId: 3, position: {lat: 48.27913750025522, lng: 14.248194694519043} },   // Leonding Stadtamt
  { projectId: 4, position: {lat: 48.3097449194934, lng: 14.284286499023438} },    // Linz Ars Electronica
  { projectId: 5, position: {lat: 47.5729, lng: 14.461} },    // Admont
  { projectId: 6, position: {lat: 48.2701, lng: 14.5765} },    // Schwertberg
  { projectId: 7, position: {lat: 48.2131, lng: 14.4752} },    // Enns
  { projectId: 8, position: {lat: 48.2499, lng: 14.6347} },    // Perg
  { projectId: 9, position: {lat: 48.337349, lng: 14.365716} },    // Perg
];
