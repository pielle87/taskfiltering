import { Project } from '../app/DAOs/project';
const ONE_DAY = 86400000;

export const PROJECTS: Project[] = [
  {
    id: 0,
    name: 'Xmas calendar',
    location: 'Rome',
    status: 'red',
    description: 'The DESCRIPTION will NOT be shown in the TASKS-view. It is only shown once because it might be very long and I am not ' +
      'sure that anybody is interested in seeing the full deescription twice.',
    details:  'short summary of the project',
    date: new Date(Date.now() + ONE_DAY * 3),
    volunteers: [{ name: 'Markus Schiller', avatar: 'assets/avatars/MarkusSchiller.png' }],
  },
  { id: 1, name: 'Garden party', location: 'Paris', status: 'green', description: 'test description', date: new Date(2020, 1, 29) },
  { id: 2, name: 'Run on the Danube', location: 'Linz', status: 'red', details: 'test description', date: new Date(2020, 2, 17) },
  { id: 3, name: 'Visit mayor', location: 'Leonding', status: 'green', description: 'test description', date: new Date(2020, 11, 17) },
  {
    id: 4,
    name: 'Redecorate AEC',
    location: 'Linz',
    status: 'amber',
    volunteers: [{ name: 'John Doe', avatar: 'assets/avatars/JohnDoe.png' }],
    date: new Date(Date.now() - ONE_DAY * 3)
  },
  {
    id: 5,
    name: 'Freiwillige Feuerwehr - Übung Brandeinsatz',
    location: 'Admont',
    date: new Date(2019, 3, 4)
  },
  {
    id: 6,
    name: 'Marsch am Pfingst-Freitag',
    location: 'Schwertberg',
    status: 'amber',
    date: new Date(2019, 4, 18),
    rating: 4,
  },
  {
    id: 7,
    name: 'Rettungssanitäter Tagesansicht Ausfahrten',
    description: 'Durchführung von eine Person; außer Termine um 15:00 und 17:00, weiterer Rettungssanitäter benötigt',
    details: 'Tagesplanung für Rettungsfahrer',
    location: 'Enns',
    date: new Date(2019, 2, 25),
    time: '07:00',
  },
  {
    id: 8,
    name: 'Rotes Kreuz Big Picture Workshop',
    location: 'Perg',
    status: 'red',
    date: new Date(2019, 0, 26),
  },
  {
    id: 9,
    name: 'Freiwillige Feuerwehr - Einsatz',
    location: 'Treffling',
    status: 'green',
    date: new Date(2019, 6, 3),
    time: '17:15'
  },
];
