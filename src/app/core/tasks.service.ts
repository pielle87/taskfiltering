import { Injectable } from '@angular/core';
import { TASKS } from './../../assets/mock-tasks';
import { Task } from '../DAOs/task';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  constructor() { }

  getAllTasks(): Task[] {
    return TASKS;
  }

  getTasksByProjectId(projectId: number): Task[] {
    return this.getAllTasks().filter(task => task.projectId === projectId);
  }

  // get volunteers names in a given task (or all tasks if not defined) as a flattened array
  getVolunteersNames(tasks: Task[] = this.getAllTasks()): string[] {
    const volunteersNames =
      tasks.map(task => task.data.volunteers && task.data.volunteers.length > 0 ? task.data.volunteers.map(vol => vol.name) : []);
    const allNames = volunteersNames.flat();
    return allNames;
  }

  getVolunteersNamesByProjectId(projectId: number): string[] {
    const tasksByProj = this.getTasksByProjectId(projectId);
    return this.getVolunteersNames(tasksByProj);
  }

  resetIsFiltered(): void {
    this.getAllTasks().forEach(task => {
      task.data.isFilteredText = false;
      task.data.isFilteredVolunteers = false;
    });
  }

  resetIsFilteredText(): void {
    this.getAllTasks().forEach(task => {
      task.data.isFilteredText = false;
    });
  }

  resetIsFilteredVolunteers(): void {
    this.getAllTasks().forEach(task => {
      task.data.isFilteredVolunteers = false;
    });
  }

  // Set the 'isFilteredText' attribute for tasks in a given project
  setIsFilteredTextForTasks(projectId: number, textData: string): void {
    const tasksByProj = this.getTasksByProjectId(projectId);

    tasksByProj.forEach(
      task =>
        task.data.isFilteredText =
        (task.name.toLowerCase().indexOf(textData.toLowerCase()) > -1) ||
        (task.data.description && task.data.description.toLowerCase().indexOf(textData.toLowerCase()) > -1) ||
        (task.data.details && task.data.details.toLowerCase().indexOf(textData.toLowerCase()) > -1)
    );
  }

  // Set the 'isFilteredVolunteers' attribute for tasks in a given project
  setIsFilteredVolunteersForTasks(projectId: number, volunteersData: string): void {
    const tasksByProj = this.getTasksByProjectId(projectId);

    tasksByProj.forEach(
      task =>
        task.data.isFilteredVolunteers =
        task.data.volunteers &&
        task.data.volunteers.length > 0 &&
        task.data.volunteers.some((vol) => vol.name.toLowerCase().includes(volunteersData.toLowerCase()))
    );
  }
}
