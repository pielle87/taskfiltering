import { Injectable } from '@angular/core';
import { Project } from '../DAOs/project';
import { PROJECTS } from './../../assets/mock-projects';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor() { }

  getAllProjects(): Project[] {
    return PROJECTS;
  }

  getProjectById(id: number): Project {
    return this.getAllProjects().find(proj => proj.id === id);
  }

  setIsFilteredTextForProject(project: Project, isProjectText: boolean): void {
    project.isFilteredText = isProjectText;
  }

  setIsFilteredVolunteersForProject(project: Project, isProjectText: boolean): void {
    project.isFilteredVolunteers = isProjectText;
  }

  resetIsFiltered(): void {
    this.getAllProjects().forEach(proj => {
      proj.isFilteredText = false;
      proj.isFilteredVolunteers = false;
    });
  }

  resetIsFilteredText(): void {
    this.getAllProjects().forEach(proj => {
      proj.isFilteredText = false;
    });
  }

  resetIsFilteredVolunteers(): void {
    this.getAllProjects().forEach(proj => {
      proj.isFilteredVolunteers = false;
    });
  }
}
