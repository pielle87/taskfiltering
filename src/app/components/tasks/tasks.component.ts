import { TasksService } from './../../core/tasks.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/app/core/project.service';
import { Project } from 'src/app/DAOs/project';
import { TreeNode } from 'primeng/api';
import { Task } from 'src/app/DAOs/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  projectId: number;
  project: Project;
  tasks: Task[];
  rootProjectTreeNode: TreeNode[];
  childrenTasksTreeNode: TreeNode[] = [];
  blueinfo = 'assets/icons/blueinfo.png';
  blueinfotitle = `Note on markup:
  - RED: matches the TEXT filter
  - ORANGE: matches the VOLUNTEERS filter
  - (NONE): no filters have been used`;

  constructor(private route: ActivatedRoute, private projService: ProjectService, private tasksService: TasksService) { }

  ngOnInit(): void {
    this.projectId = +this.route.snapshot.paramMap.get('id'); // cast to number with '+'
    this.project = this.projService.getProjectById(this.projectId);
    this.tasks = this.tasksService.getTasksByProjectId(this.projectId);
    this.createRootProjectTreeNode();
    this.populateAndPushChildrenTasksTreeNode();
  }

  // The root node is always the project
  createRootProjectTreeNode(): void {
    if (this.project != null) {
      this.rootProjectTreeNode = [{
        label: this.project.name,
        type: 'tasktype',
        styleClass: 'ui-taskstyle',
        expanded: true,
        data: this.project,
        children: []
      }];
    }
  }

  // Tasks are added as children to the root and to other tasks
  populateAndPushChildrenTasksTreeNode(): void {
    this.tasks.forEach(task => {
      const childTreeNode: TreeNode = {
        label: task.name,
        type: 'tasktype',
        styleClass: 'ui-taskstyle',
        expanded: true,
        data: task.data,
        children: []
      };
      /* NOTE: it is working because INTERNALLY my IDs match the array IDs BUT it is not really clear why (but I can change
       * the IDs in my DB and it is still working).
       * BEST would be to use the taskId somewhere (populate the Tree using the index of the task? 0 is the root and every other
       * position matches the task?)
       * Also, I could use -1 for the project, that'd make it clearer but would imply other changes because -1 is not a valid index */
      this.childrenTasksTreeNode.push(childTreeNode);
      if (task.parentTaskId === 0) {
        this.rootProjectTreeNode[task.parentTaskId].children.push(childTreeNode);
      } else {
        this.childrenTasksTreeNode[task.parentTaskId - 1].children.push(childTreeNode);
      }
    });
  }
}
