import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksComponent } from './tasks.component';
import { AppRoutingModule } from './../../app-routing.module';
import { TasksDetailComponent } from './tasks-detail/tasks-detail.component';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  declarations: [
    TasksComponent,
    TasksDetailComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    OrganizationChartModule,
    DialogModule
  ],
  exports: [TasksComponent],
})
export class TasksModule { }
