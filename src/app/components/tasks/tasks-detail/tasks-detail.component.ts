import { Volunteer } from '../../../DAOs/volunteer';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { TreeNode } from 'primeng/api';

@Component({
  selector: 'app-tasks-detail',
  templateUrl: './tasks-detail.component.html',
  styleUrls: ['./tasks-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TasksDetailComponent implements OnInit {
  @Input() tasksTreeNode: TreeNode[];
  selectedNode: TreeNode;
  display = false;
  taskProps: String[];
  taskValues: String[];
  imageSrc: String = 'assets/avatars/JohnDoe.png';
  imageAlt = 'avatar';
  volunteers: Volunteer[] = null;
  projectDate: Date;
  taskRating: number;
  ratingArray: number[] = [];
  maxRating = 5;

  constructor() { }

  ngOnInit(): void { }

  fillRatingArray(rating: number) {
    const tempArray: number[] = [];
    for (let index = 0; index < this.maxRating; index++) {
      if (index < rating) {
        tempArray.push(1);
      } else {
        tempArray.push(0);
      }
    }
    return tempArray;
  }

  onNodeSelect(event): void {
    // 1. first of all, get the specific key-values that need special handling and perform all other needed code
    this.volunteers = event.node.data.volunteers;
    this.projectDate = event.node.data.date;
    this.taskRating = event.node.data.rating;
    this.ratingArray = this.fillRatingArray(event.node.data.rating);
    // 2. then, remove some specific key-values which we do NOT want to display
    const keysToHide: string[] = ['id', 'date', 'volunteers', 'isFilteredText', 'isFilteredVolunteers', 'rating'];
    // 3. get all other properties, to be displayed in a loop
    this.taskProps = Object.keys(event.node.data).filter(key => !keysToHide.includes(key));
    this.taskValues = this.taskProps.map((key: string) => event.node.data[key]);
    // 4. set the display variable to true
    this.display = true;
  }
}
