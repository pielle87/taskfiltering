import { MapMarker } from '../../../DAOs/map-marker';
import { MARKERS } from './../../../../assets/mock-map-markers';
import { Component, OnInit, ViewChild, Input, AfterViewInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { GoogleMap } from '@angular/google-maps';
import { Project } from 'src/app/DAOs/project';

@Component({
  selector: 'app-projects-map',
  templateUrl: './projects-map.component.html',
  styleUrls: ['./projects-map.component.css']
})
export class ProjectsMapComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild(GoogleMap) googleMap: GoogleMap;
  @Input() filteredProjects: Project[];
  @Input() doRefreshBounds: boolean;
  @Output() changedBounds: EventEmitter<number[]> = new EventEmitter<number[]>();

  viewInitialized = false;
  filteredProjectsIds: number[] = [];
  height = '500px';
  center: google.maps.LatLngLiteral;
  latlngBounds = new google.maps.LatLngBounds();
  displayedMarkers: MapMarker[] = MARKERS;
  zoom = 14;

  constructor() { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.displayedMarkers.forEach(marker => this.latlngBounds.extend(marker.position));
    this.googleMap.fitBounds(this.latlngBounds);
    this.viewInitialized = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges, refreshBounds: ' + this.doRefreshBounds);
    this.filteredProjectsIds = this.filteredProjects.map(project => project.id);
    this.displayedMarkers = MARKERS.filter(marker => this.filteredProjectsIds.includes(marker.projectId));
    if (this.viewInitialized && this.doRefreshBounds) {
      console.log('re-initialize bounds and refit view');
      this.latlngBounds = new google.maps.LatLngBounds();
      this.displayedMarkers.forEach(marker => this.latlngBounds.extend(marker.position));
      this.googleMap.fitBounds(this.latlngBounds);
    }
  }

  onBoundsChanged(): void {
    console.log('onBoundsChanged, refreshBounds: ' + this.doRefreshBounds);
    const ne: google.maps.LatLng = this.googleMap.getBounds().getNorthEast();
    const sw: google.maps.LatLng = this.googleMap.getBounds().getSouthWest();
    // Upon dragging (NO refresh): update markers to include them all again
    if (!this.doRefreshBounds) {
      this.displayedMarkers = MARKERS;
    }
    this.displayedMarkers = this.displayedMarkers.filter(pos =>
      (ne.lat() >= pos.position.lat) && (sw.lat() <= pos.position.lat) &&
      (sw.lng() <= pos.position.lng) && (ne.lng() >= pos.position.lng)
    );
    console.log('fire event changedBounds');
    this.changedBounds.emit(this.displayedMarkers.map(marker => marker.projectId));
  }
}
