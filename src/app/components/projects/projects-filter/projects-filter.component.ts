import { TasksService } from './../../../core/tasks.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/DAOs/project';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-projects-filter',
  templateUrl: './projects-filter.component.html',
  styleUrls: ['./projects-filter.component.css'],
  providers: [DatePipe]
})
export class ProjectsFilterComponent implements OnInit {

  @Input() filteredProjects: Project[];
  @Output() changedText: EventEmitter<string> = new EventEmitter<string>();
  @Output() changedLocation: EventEmitter<string> = new EventEmitter<string>();
  @Output() changedStatus: EventEmitter<string> = new EventEmitter<string>();
  @Output() changedVolunteers: EventEmitter<string> = new EventEmitter<string>();
  @Output() changedDate: EventEmitter<Date> = new EventEmitter<Date>();
  uniqueLocations: string[];
  uniqueStatus: string[];
  uniqueVolunteers: string[];
  displayedVolunteers: string[];

  private _textFilter: string;
  get textFilter() {
    return this._textFilter;
  }

  set textFilter(val: string) {
    this._textFilter = val;
    console.log('emit event textFilter: ' + this._textFilter);
    this.changedText.emit(this._textFilter);
  }

  private _locationFilter: string;
  get locationFilter() {
    return this._locationFilter;
  }

  set locationFilter(val: string) {
    this._locationFilter = val;
    console.log('emit event locationFilter: ' + this._locationFilter);
    this.changedLocation.emit(this._locationFilter);
  }

  private _statusFilter: string;
  get statusFilter() {
    return this._statusFilter;
  }

  set statusFilter(val: string) {
    this._statusFilter = val;
    console.log('emit event statusFilter: ' + this._statusFilter);
    this.changedStatus.emit(this._statusFilter);
  }

  private _volunteersFilter: string;
  get volunteersFilter() {
    return this._volunteersFilter;
  }

  set volunteersFilter(val: string) {
    this._volunteersFilter = val;
    console.log('emit event volunteersFilter: ' + this._volunteersFilter);
    this.changedVolunteers.emit(this._volunteersFilter);
  }

  private _dateFilter: Date = null;
  get dateFilter() {
    return this._dateFilter;
  }

  set dateFilter(val: Date) {
    this._dateFilter = val;
    console.log('emit event dateFilter: ' + this._dateFilter);
    this.changedDate.emit(this._dateFilter);
  }

  constructor(private tasksService: TasksService) { }

  ngOnInit(): void {
    this.uniqueLocations = [...new Set(this.filteredProjects.map(project => project.location))];
    this.uniqueVolunteers = [...new Set(this.tasksService.getVolunteersNames())];
    this.displayedVolunteers = this.uniqueVolunteers;
    // this is an optional value: so filter before mapping in order to remove the redundant '' value (already covered by the "Any" case)
    this.uniqueStatus = [...new Set(this.filteredProjects.filter(project => project.status).map(project => project.status))];
  }

  updateAutocompleteVolunteerModel(): void {
    this.displayedVolunteers =
      this.uniqueVolunteers.filter(name => name.toLowerCase().indexOf(this._volunteersFilter.toLowerCase()) > -1);
  }
}
