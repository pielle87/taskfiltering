import { TasksService } from './../../core/tasks.service';
import { ProjectService } from 'src/app/core/project.service';
import { Component, OnInit } from '@angular/core';
import { Project } from '../../DAOs/project';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  allProjects: Project[];
  filteredProjects: Project[];
  doRefreshBounds = false;
  isShowMapSelected = false;
  textData = '';
  locationData = '';
  statusData = '';
  volunteersData = '';
  dateData: Date = null;

  constructor(private projectService: ProjectService, private tasksService: TasksService) {
  }

  ngOnInit(): void {
    this.allProjects = this.projectService.getAllProjects();
    this.filteredProjects = this.allProjects;
    this.resetIsFiltered();
  }

  textFilter(data: string): void {
    this.textData = data;
    this.applyFilters();
  }

  locationFilter(data: string): void {
    this.locationData = data;
    this.applyFilters();
  }

  statusFilter(data: string): void {
    this.statusData = data;
    this.applyFilters();
  }

  volunteersFilter(data: string): void {
    this.volunteersData = data;
    this.applyFilters();
  }

  dateFilter(data: Date): void {
    this.dateData = data;
    this.applyFilters();
  }

  mapFilter(ids: number[]): void {
    console.log('MAP FILTER');
    // do NOT refresh the map bounds when dragging and zooming
    this.doRefreshBounds = false;
    this.filteredProjects = this.allProjects.filter(project => ids.includes(project.id));
  }

  // Reset the frontend attribute used to highlight the projects
  resetIsFiltered(): void {
    this.tasksService.resetIsFiltered();
    this.projectService.resetIsFiltered();
  }

  findInString(reference: string, partialSearchTerm: string): boolean {
    return reference.toLowerCase().includes(partialSearchTerm.toLowerCase());
  }

  // check whether any of the filters is being used (NOTE: the map does NOT count as a filter)
  isAnyFilterActive(): boolean {
    return (this.textData !== '') || (this.locationData !== '') || (this.statusData !== '') ||
      (this.volunteersData !== '') || (this.dateData !== null);
  }

  applyFilters(): void {
    // refresh the map bounds when using those filters (recenter, rezoom)
    this.doRefreshBounds = true;

    if (this.isAnyFilterActive()) {
      // get the list of filtered projects based on the current filters values
      console.log('TEXT DATA: ' + this.textData + ' LOCATION DATA: ' + this.locationData +
        ' STATUS DATA: ' + this.statusData + ' VOLUNTEERS DATA: ' + this.volunteersData + ' DATE DATA: ' + this.dateData);

      // filter the projects and return only the ones for which ALL the filtering-options are currently TRUE
      this.filteredProjects = this.allProjects.filter((proj: Project) => {

        // TEXT FILTER
        // (NOTE: check first 'textData' to skip 'expensive' calls. Also: 'description' and 'details' are optional so check if they exist)
        let isText = false;
        if (this.textData === '') {
          isText = true;
          this.projectService.resetIsFilteredText();
          this.tasksService.resetIsFilteredText();
        } else {
          const isProjectText: boolean = this.findInString(proj.name, this.textData) ||
            (proj.description && this.findInString(proj.description, this.textData)) ||
            (proj.details && this.findInString(proj.details, this.textData));
          const isTaskText: boolean = this.tasksService.getTasksByProjectId(proj.id).some(task =>
            this.findInString(task.name, this.textData) ||
            (task.data.description && this.findInString(task.data.description, this.textData)) ||
            (task.data.details && this.findInString(task.data.details, this.textData))
          );

          isText = isProjectText || isTaskText;
          if (isText) {
            // NOTE: I already have the project so I could just manipulate the data. BUT I'm setting stuff so let's do it in the service!
            this.projectService.setIsFilteredTextForProject(proj, isProjectText);
            this.tasksService.setIsFilteredTextForTasks(proj.id, this.textData);
          }
        }

        // LOCATION FILTER
        const isLocation = (this.locationData === '') || this.findInString(proj.location, this.locationData);

        // STATUS FILTER (NOTE: 'status' is optional so check if it exists)
        const isStatus = (this.statusData === '') || (proj.status && this.findInString(proj.status, this.statusData));

        // VOLUNTEERS FILTER (NOTE: check first 'volunteersData' to maybe skip the 'expensive' calls)
        let isVolunteers = false;
        if (this.volunteersData === '') {
          isVolunteers = true;
          this.projectService.resetIsFilteredVolunteers();
          this.tasksService.resetIsFilteredVolunteers();
        } else {
          const isProjectVolunteers: boolean = proj.volunteers && proj.volunteers.length > 0 &&
            proj.volunteers.some(vol => this.findInString(vol.name, this.volunteersData));
          const isTaskVolunteers: boolean = this.tasksService.getVolunteersNamesByProjectId(proj.id)
            .some(name => this.findInString(name, this.volunteersData));

          isVolunteers = isProjectVolunteers || isTaskVolunteers;
          if (isVolunteers) {
            this.projectService.setIsFilteredVolunteersForProject(proj, isProjectVolunteers);
            this.tasksService.setIsFilteredVolunteersForTasks(proj.id, this.volunteersData);
          }
        }

        // DATE FILTER
        const isDate = proj.date >= this.dateData;

        return isText && isLocation && isStatus && isVolunteers && isDate;
      });
    } else {
      this.filteredProjects = this.allProjects;
      this.resetIsFiltered();
    }
  }
}
