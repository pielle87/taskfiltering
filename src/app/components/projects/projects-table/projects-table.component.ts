import { Project } from '../../../DAOs/project';
import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-projects-table',
  templateUrl: './projects-table.component.html',
  styleUrls: ['./projects-table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0px' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ProjectsTableComponent implements OnInit, OnChanges {
  @Input() filteredProjects: Project[];
  displayedColumns: string[] = ['status', 'name', 'location', 'date'];
  dataSource: MatTableDataSource<Project>;
  expandedElement: Project | null;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    this.dataSource = new MatTableDataSource(this.filteredProjects);
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    // see https://stackoverflow.com/a/34568344/1469932 --> an @Input can only be used after it is initialized
  }
}
