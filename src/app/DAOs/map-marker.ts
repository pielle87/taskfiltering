export interface MapMarker {
  projectId: number;
  position: { lat: number; lng: number };
}
