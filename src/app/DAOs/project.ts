import { Volunteer } from './volunteer';

// NOTE: a lot of attributes are in common with Task and TaskData. Maybe there was a better solution here.
export interface Project {
  id: number;
  name: string;
  status?: string;
  details?: string;
  description?: string;
  location: string;
  date: Date;
  volunteers?: Volunteer[];
  comments?: string;
  materials?: string;
  time?: string;
  isFilteredVolunteers?: boolean;
  isFilteredText?: boolean;
  rating?: number;
}
