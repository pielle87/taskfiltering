import { TaskData } from './task-data';

export interface Task {
  projectId: number;
  taskId: number;
  parentTaskId: number;
  name: string;
  data: TaskData;
}
