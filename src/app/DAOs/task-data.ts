import { Volunteer } from './volunteer';

export interface TaskData {
  details?:  string;
  description?: string;
  location?: string;
  date?: Date;
  volunteers?: Volunteer[];
  comments?: string;
  materials?: string;
  time?: string;
  isFilteredVolunteers?: boolean;
  isFilteredText?: boolean;
  rating?: number;
}
